==========================
Plej granda prima obligero
==========================

-----------
Problemo_ 3
-----------


La primaj obligeroj de 13195 estas 5, 7, 13 and 29.

Kiu estas la plej granda prima obligero de nombro 600851475143 ?


.. footer::

    Copyright 2020 Cristian F. GUAJARDO YÉVENES. https://gitlab.com/cfgy/speuler

    |cc:by-nc-sa/4.0|_ Mi liberigas ĉi tiun tekston per permesilo
    *Krea Komunaĵo Atribuite-Nekomerce-Samkondiĉe 4.0 Tutmonda*
    `CC:BY-NC-SA/4.0`_.

    La originala teksto de la problemo_, skribita en la angla, estis konigita ĉe
    `Project Euler`_  kaj liberigita_ per permesilo `CC:BY-NC-SA/4.0`_.


.. ligiloj

.. _`Project Euler`: https://projecteuler.net/
.. _problemo: https://projecteuler.net/problem=3
.. _liberigita: https://projecteuler.net/copyright

.. _`CC:BY-NC-SA/4.0`: https://creativecommons.org/licenses/by-nc-sa/4.0/


.. bildetoj: 88x31 (granda), 80x15 (malgranda)

.. |cc:by-nc-sa/4.0| image:: https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png
