Respondoj
=========

Respondoj al la problemoj de `Project Euler`_.

======== =========
Problemo Respondo
======== =========
0001_    233168
0002_    4613732
0003_    6857
======== =========

.. problemoj

.. _0001: https://projecteuler.net/problem=1
.. _0002: https://projecteuler.net/problem=2
.. _0003: https://projecteuler.net/problem=3


.. footer::

    Copyright 2020 Cristian F. GUAJARDO YÉVENES. https://gitlab.com/cfgy/speuler

    |cc:by-sa/4.0|_ Mi liberigas ĉi tiun tekston per permesilo
    *Krea Komunaĵo Atribuite-Samkondiĉe 4.0 Tutmonda* `CC:BY-SA/4.0`_.


.. ligiloj

.. _`Project Euler`: https://projecteuler.net/
.. _`CC:BY-SA/4.0`: https://creativecommons.org/licenses/by-sa/4.0/


.. bildetoj: 88x31 (granda), 80x15 (malgranda)

.. |cc:by-sa/4.0| image:: https://i.creativecommons.org/l/by-sa/4.0/88x31.png
