================
Obloj de 3 kaj 5
================

-----------
Problemo_ 1
-----------


Se ni vicigas ĉiujn naturajn numerojn sub 10, tiuj kiuj estas obloj de 3 aŭ 5,
ni ricevas 3, 5, 6 kaj 9. La sumo de ĉi tiuj obloj estas 23.

Trovu la sumon de ĉiuj obloj de 3 aŭ 5 sub 1000.


.. footer::

    Copyright 2020 Cristian F. GUAJARDO YÉVENES. https://gitlab.com/cfgy/speuler

    |cc:by-nc-sa/4.0|_ Mi liberigas ĉi tiun tekston per permesilo
    *Krea Komunaĵo Atribuite-Nekomerce-Samkondiĉe 4.0 Tutmonda*
    `CC:BY-NC-SA/4.0`_.

    La originala teksto de la problemo_, skribita en la angla, estis konigita ĉe
    `Project Euler`_  kaj liberigita_ per permesilo `CC:BY-NC-SA/4.0`_.


.. ligiloj

.. _`Project Euler`: https://projecteuler.net/
.. _problemo: https://projecteuler.net/problem=1
.. _liberigita: https://projecteuler.net/copyright

.. _`CC:BY-NC-SA/4.0`: https://creativecommons.org/licenses/by-nc-sa/4.0/


.. bildetoj: 88x31 (granda), 80x15 (malgranda)

.. |cc:by-nc-sa/4.0| image:: https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png
