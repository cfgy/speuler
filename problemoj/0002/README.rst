======================
Paraj Fibonaĉi-nombroj
======================

-----------
Problemo_ 2
-----------


Ĉiu nova vicero de Fibonaĉi vico naskiĝas el sumigo de la du antaŭan viceroj.
Komencante de nombroj 1 kaj 2, la unuaj 10 viceroj estos:

.. math::
    1, 2, 3, 5, 8, 13, 21, 34, 55, 89, \ldots

Konsiderante viceroj de Fibonaĉi vico kies valoroj ne preterpasas kvar milionoj,
trovu la sumon de paraj viceroj.


.. footer::

    Copyright 2020 Cristian F. GUAJARDO YÉVENES. https://gitlab.com/cfgy/speuler

    |cc:by-nc-sa/4.0|_ Mi liberigas ĉi tiun tekston per permesilo
    *Krea Komunaĵo Atribuite-Nekomerce-Samkondiĉe 4.0 Tutmonda*
    `CC:BY-NC-SA/4.0`_.

    La originala teksto de la problemo_, skribita en la angla, estis konigita ĉe
    `Project Euler`_  kaj liberigita_ per permesilo `CC:BY-NC-SA/4.0`_.


.. ligiloj

.. _`Project Euler`: https://projecteuler.net/
.. _problemo: https://projecteuler.net/problem=2
.. _liberigita: https://projecteuler.net/copyright

.. _`CC:BY-NC-SA/4.0`: https://creativecommons.org/licenses/by-nc-sa/4.0/


.. bildetoj: 88x31 (granda), 80x15 (malgranda)

.. |cc:by-nc-sa/4.0| image:: https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png
