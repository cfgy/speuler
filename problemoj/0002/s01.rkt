#|
Copyright 2020 Cristian F. GUAJARDO YÉVENES. https://gitlab.com/cfgy/speuler.

This file is part of SPEuler.

SPEuler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SPEuler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SPEuler.  If not, see <https://www.gnu.org/licenses/>.
|#


#|
Solvo de problemo 2

Vicigi parajn fibonaĉian nombrojn kaj sumigas ilin.
|#


#lang racket/base

(require racket/list)


;; funkcioj

(define (list_fibonacci [ks empty] #:from fs #:below n #:take take?)
  (define ls (cond [(empty? ks) (filter take? fs)]
                   [else ks]))
  (define next (apply + fs))
  (cond [(>= next n) ls]
        [else (list_fibonacci (cond [(take? next) (cons next ls)]
                                    [else ls])
                              #:from (list next (first fs))
                              #:below n
                              #:take take?)]))


;; ĉefa programo

(define n 4000000)
(define even (list_fibonacci #:from (list 2 1) #:below n #:take even?))
(apply + even)
