=======
SPEuler
=======

SPEuler, laŭ la prononco *spoiler*, signifas *Solvoj de la Projekto Euler*.

Ĉi tiu projekto enhavas miajn solvojn al problemoj konigitaj ĉe `Project Euler`_.


Similaj projektoj
=================

.. [Denton]
    Zach DENTON.
    *Project Euler Solutions*.
    https://zach.se/project-euler-solutions/

.. [Dreamshire]
    Dreamshire.
    *Project Euler 1 Solution*.
    https://blog.dreamshire.com/project-euler-1-solution/

.. [Li]
    Bai LI.
    *Project Euler Solutions*.
    https://github.com/luckytoilet/projecteuler-solutions

.. [MathBlogDK]
    mathblog.dk.
    *Project Euler Solutions*
    https://www.mathblog.dk/project-euler-solutions/

.. [Nayuki]
    Project Nayuki.
    *Project Euler Solutions*.
    https://www.nayuki.io/page/project-euler-solutions


.. footer::

    Copyright 2020 Cristian F. GUAJARDO YÉVENES. https://gitlab.com/cfgy/speuler

    |cc:by-sa/4.0|_ Mi liberigas ĉi tiun tekston per permesilo
    *Krea Komunaĵo Atribuite-Samkondiĉe 4.0 Tutmonda* `CC:BY-SA/4.0`_.


.. ligiloj

.. _`Project Euler`: https://projecteuler.net/
.. _`CC:BY-SA/4.0`: https://creativecommons.org/licenses/by-sa/4.0/


.. bildetoj: 88x31 (granda), 80x15 (malgranda)

.. |cc:by-sa/4.0| image:: https://i.creativecommons.org/l/by-sa/4.0/88x31.png
